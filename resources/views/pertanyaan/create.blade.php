@extends('adminlte.master')

@section('content')

<div class="container">
    <form action="/pertanyaan" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="judul" id="title" placeholder="Masukkan Judul">
            @error('title') 
                <div class="alert alert-danger">
                    {{message}}
                </div>
            @enderror
        </div>
        <div class="form-group">
        <label for="body">Isi</label>
            <textarea class="form-control" name="isi" id="body" cols="30" rows="10"></textarea>
            @error('body') 
                <div class="alert alert-danger">
                    {{message}}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>

@endsection